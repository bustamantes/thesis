%##################################################################################################
\chapter{Structure formation}
\label{cha:1_structure_formation}
%##################################################################################################

Our current standard paradigm of structure formation in the Universe encompasses and links many theories that describe different phenomena over a vast range of dynamical scales. At the largest scales of the Universe, the $\Lambda$CDM cosmology accounts for the evolution of the homogeneous and isotropic space-time. During the first stages of structure formation, the cosmological linear theory describes the evolution of the matter perturbations that later on give rise to the first galaxies. At the onset of the non-linear regime, during the so-called quasi-linear regime, the Zeldovich approximation provides a way to compute the velocity and position fields of the perturbations, which serve as initial condition in simulations of galaxy formation. In this chapter, we briefly outline each of these aspects. We refer the reader to the books of \citet{ padmanabhan1995}, \cite{Longair2008} and \citet{Mo2010} for more in-depth details.

%==================================================================================================
\section{$\boldsymbol{\Lambda}$CDM cosmology}
\label{sec:LCDMCosmology}
%==================================================================================================

The two main pillars of the $\Lambda$CDM cosmology are the cosmological principle \citep[e.g.][]{Carroll2006} and the theory of general relativity \citep{Einstein1916}. The cosmological principle establishes the notion that our position in the Universe is not privileged with respect to any other position. This picture is supported by observations of the CMB \citep{Smoot1992, Spergel2003, Planck2011}, therefore, it can be reasonably assumed that the Universe is highly isotropic (invariant under rotations) and homogeneous (invariant under translations) at large scales ($\gtrsim 100\, \mathrm{Mpc}$). As discussed in chapter~\ref{cha:0_introduction}, primordial perturbations can grow and eventually form galaxies, which are inherently non-homogeneous and non-isotropic features in the matter field, breaking thus the above assumption at smaller scales. Different theoretical treatments have to be, therefore, adopted for studying the large-scale and small-scale Universe. We devote this section to the description of the large-scale Universe.

The theory of general relativity provides the mathematical framework to describe the geometry of the Universe and link it to the content of matter and energy through gravity. The mathematical description of a curved space-time is given by a manifold in which distances are determined by a metric tensor $g_{\mu \nu}$ \citep[e.g.][]{Carroll2006}. The invariant line element $ds$ can be expressed as:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{LineElement}
{ ds^2 = g_{\mu\nu}dx^{\mu}dx^{\nu}, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $dx^{\mu}$ and $dx^{\nu}$ ($\mu,\, \nu = 0,1,2,3$) are contravariant infinitesimal displacement four-vectors and $dx^0 \equiv cdt$, with $c$ being the constant speed of light in vacuum. Einstein summation notation is adopted, i.e. contravariant and covariant quantities with the same index represent a summation. Given the invariant nature of the line element, i.e. it does not depend on the adopted coordinate system, the distance between two points can be computed by integrating the different line elements that connect them.

%.........................................................................
%	Figure 1
%.........................................................................
%Carton of Universe geometries
\begin{figure*}
\centering
\includegraphics[width=0.6\textwidth]
{./figures/1_structure_formation/Universe_Geometry.pdf}
\caption{\small{Curved spaces according to the curvature parameter. Flat geometry for $k=0$ (\emph{left}), open geometry for $k=1$ (\emph{centre}) and closed geometry for $k=-1$ (\emph{right}).}}
\label{fig:CurvedSpace}
\end{figure*}
%.........................................................................

In order to derive the components of the metric tensor, we consider the condition of isotropy of the Universe, which enforces that the components connecting two different spatial coordinates vanish, i.e. $g_{ij} = 0$ for $i\neq j$ and $i,j = 1,2,3$. Likewise, components connecting spatial coordinates with the time coordinate should also vanish as, otherwise, a preferred direction in space will have a different evolution than the others. Under these conditions, a convenient way to express the metric is the following:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{MetricCoord}
{ ds^2 = -c^2d^2 + a^2(t)\left[ \frac{dr^2}{1 - kr^2} + r^2(d\theta^2 + \sin^2\theta d\phi^2) \right], }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $a(t)$ is the scale factor that determines the isotropic expansion of the whole Universe as a function of time, $(r,\phi,\theta)$ are the spherical coordinates, and $k$ is the curvature parameter that quantifies the geometry of the Universe, e.g. $k=0$, $k=-1$ and $k=1$ correspond to a flat, closed and open universe, respectively (see Figure~\ref{fig:CurvedSpace}). Defining the \textit{eigentime} $\tau$ of a point as the elapsed time in the rest-frame of that point, i.e. $ds^2 \equiv -c^2 d\tau^2$, and considering that in such a configuration, the system time $t$ coincides with $\tau$, we conclude that $g_{00} = -1$. The tensor metric is then given by:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{MetricCoord}
{ g_{\mu\nu} = \mathrm{diag}\left( -1,\ \frac{a^2(t)}{1-kr^2},\ a^2(t)r^2,\ a^2(t)r^2\sin^2\theta \right). }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

This is the so-called Friedmann-Lemaitre-Robertson-Walker (FLRW) metric and is the basis for solving the Einstein field equation. As mentioned above, the theory of general relativity links the geometry of the Universe, parameterised via the Einstein tensor $G_{\mu\nu}$, to the matter and energy content described by the stress-energy tensor $T_{\mu\nu}$ via the Einstein field equation:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{EinsteinFieldEquation}
{ G_{\mu\nu} = R_{\mu\nu} - \frac{1}{2}g_{\mu\nu}R = \frac{8\pi GT_{\mu\nu}}{c^2} - g_{\mu}\Lambda, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $G$ is the gravitational constant and $\Lambda$ is the cosmological constant. The quantities $R_{\mu\nu}$ and $R$ correspond to the Ricci tensor and Ricci scalar, respectively, and are defined as:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\begin{eqnarray}
 \Gamma^\mu_{\alpha\beta} &=& \frac{g^{\mu\nu}}{2}\left[ \frac{\partial g_{\alpha \nu}}{\partial x^\beta} + \frac{\partial g_{\beta \nu}}{\partial x^\alpha} - \frac{\partial g_{\alpha \beta}}{\partial x^\nu} \right], \\
 \nonumber
 \\
 R_{\mu\nu} &=& \frac{\partial \Gamma^{\alpha}_{\mu\nu}}{\partial x^\alpha} - \frac{\partial \Gamma^{\alpha}_{\mu\alpha}}{\partial x^\nu} + \Gamma^{\alpha}_{\beta\alpha}\Gamma^{\beta}_{\mu\nu} - \Gamma^{\alpha}_{\beta\nu}\Gamma^{\beta}_{\mu\alpha}, \\
 \nonumber
 \\
 R &=& g^{\mu\nu}R_{\mu\nu}.
\end{eqnarray}
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

Approximating the behaviour at large-scales of the different components of the Universe as to an ideal fluid, the stress-energy tensor for each component can be written as:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{StressEnergyTensor}
{ T_{\mu\nu} = \left( \rho + \frac{P}{c^2} \right)v_{\mu}v_{\nu} + Pg_{\mu\nu}, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
with $P$ denoting the pressure, $\rho$ the density and $v$ the velocity of the fluid. By combining the previous equations for the FLRW metric, the Einstein tensor and the stress-energy tensor, the Einstein field equation can be readily solved, yielding thus the Friedmann equations \citep{Friedmann1922}:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\begin{eqnarray}
 \left( \frac{\dot a}{a} \right)^2 &=& \frac{8\pi G}{3}\rho - \frac{kc^2}{a^2} + \frac{\Lambda c^2}{3},
 \nonumber
 \\
 \label{eq:FriedmannEq}
 \left( \frac{\ddot a}{a} \right) &=& -\frac{4\pi G}{3}\left( \rho + \frac{3p}{c^2} \right) + \frac{\Lambda c^2}{3}.
\end{eqnarray}
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

In order to solve this system of equations for the scale factor $a(t)$, it is necessary to determine the explicit time-dependence of the density $\rho$ and the pressure $P$ for each component of the Universe\footnote{Note that these quantities can only depend on time, as otherwise, the assumption of an isotropic universe would be violated.}. Detailed derivations of the time-dependent functions can be found in the book of \citet{Longair2008}. We simply list them in Table~\ref{tab:PropertiesDependence}.
%.........................................................................
%Table of dependences of Matter-Energy content of the universe with a
\begin{table}
\centering
\begin{tabular}{c | c | c | c }
\textbf{Property} 	& 
\textbf{Density} 	&
 \textbf{Pressure}	& 
\textbf{Temperature}		\\ \hline

& & &  \\
\textbf{Matter}& $\rho = \rho_{m0} a^{-3}(t)$ & $P = P_{m0} a^{-5}(t)$ & $T = T_{m0} a^{-2}(t)$ \\ 
\small{(baryonic + dark)} & & &  \\
& & &  \\
\textbf{Radiation }& $\rho = \rho_{r0} a^{-4}(t)$ & $P = P_{r0} a^{-4}(t)$ & $T = T_{r0} a^{-1}(t)$ \\ 
\small{(+ relativistic matter)} & & &  \\
& & &  \\
\textbf{Vacuum }& $\rho = \rho_{\Lambda 0} $ & $P = P_{\Lambda 0} = -\rho_{\Lambda 0}c^2 $ & $-$ \\ 
& & &  \\ \hline
\end{tabular}
\caption{Functions of density, pressure and temperature for different components of the Universe in therms of the scale factor $a(t)$ \citep{Longair2008}. Reference values $\rho_0$, $P_0$ and $T_0$ correspond to values at the current time $t_0$, at which, by convention, $a_0 = a(t_0) = 1$.}
\label{tab:PropertiesDependence}
\end{table}
%.........................................................................

We define the Hubble parameter $H(t)$ as the normalised derivative of the scale factor:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{HubbleParameter}
{ H(t)\equiv \frac{\dot a}{a}. }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

Note that at the current time, $H(t_0)=H_0$ corresponds to the expansion rate of the local Universe, which is the proportionality constant in Hubble's law \citep{Hubble1929}:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{HubblesLaw}
{ cz = H_0d, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $d$ is the physical distance from an observer (e.g. Earth) to a distant source, and $z$ is the cosmological redshift that measures the Doppler shift in wavelength that light undergoes while travelling from the distant source to the observer. The redshift can be related to the scale factor as:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{Redshift_FactorScale}
{ a(t) = \frac{1}{1+z}. }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

We define the present-day density parameters of matter, radiation and vacuum energy as:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\begin{eqnarray}
\Omega_m &\equiv& \frac{\rho_{m0}}{\rho_c}, \\
\Omega_r &\equiv& \frac{\rho_{r0}}{\rho_c}, \\
\Omega_\Lambda &\equiv& \frac{\Lambda}{3H^2_0}, \\
\Omega_0 &\equiv& \Omega_r + \Omega_r + \Omega_\Lambda, \\
\rho_c &\equiv& \frac{3H_0^2}{8\pi G},
\end{eqnarray}
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\rho_c$ is the reference critical density and is defined as the density in a static ($\Lambda=0$), flat ($k=0$) universe.

Making use of the previous definitions, the system of equations~(\ref{eq:FriedmannEq}) can be finally rewritten in the following form:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{Final_Friedmann}
{ H^2(t) = H_0^2\left[ \Omega_r(1+z)^4 + \Omega_m(1+z)^3 +(1-\Omega_0)(1+z)^2 + \Omega_\Lambda \right]. }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

The most recent measurements of the cosmological parameters are \citep{Planck2018}: $H_0 = 67.4\pm 0.5\, \mathrm{s}^{-1}\mathrm{Mpc}^{-1}\mathrm{km}$, $\Omega_m = 0.315 \pm 0.007$, $\Omega_\Lambda = 0.692 \pm 0.012$, $\Omega_r \approx 0$ and $\Omega_0 \approx 1$. According to these values, our present-day Universe is spatially flat and is dominated by the cosmological constant that causes an accelerated expansion. In analogy with the positive pressure exerted by the matter and radiation content of the Universe, if the cosmological constant is associated to a hypothetical energy field, the corresponding pressure will be negative (see Table~\ref{tab:PropertiesDependence}). This energy is often referred to as dark energy \citep{Riess1998}. The content of non-relativistic matter is in turn divided into a baryonic component and a non-baryonic component. The latter corresponds to the already-discussed cold dark matter and accounts for $84.2\%$ of the total matter content. Baryonic matter only amounts for $15.8\%$, playing thus a comparatively weaker role in the evolution of the large-scale Universe. Nevertheless, the rich and complex plethora of interactions exhibited by baryonic matter makes it the most relevant and interesting component at small scales.


%==================================================================================================
\section{Linear regime}
\label{sec:LinearRegime}
%==================================================================================================

In the previous section, we described the large-scale Universe under the assumptions of isotropy and homogeneity. As discussed in chapter~\ref{cha:0_introduction}, these assumptions do no longer hold at smaller scales owing to the growth of matter fluctuations and subsequent structure formation. Nevertheless, it is still possible to describe the evolution of the early Universe at the smaller scales by treating the growing fluctuations as small perturbations of the homogeneous and isotropic solution. This is the so-called linear regime of structure formation. In order to adopt this approach, some assumptions have to be made first, namely: the different modes of the matter field evolve independently from each other, i.e. they are linearly independent; density fluctuations are very small with respect to the background density $\delta\rho \ll \bar \rho$; and the characteristic physical size of the perturbations is smaller than the Hubble radius\footnote{The Hubble radius $r_H$ is a length scale that approximates the radius of the observable Universe.} $r_\delta \ll r_{H} \sim cH_0^{-1}$. The last assumption allows us to neglect relativistic effects of the curvature of the space-time and therefore, use a Newtonian framework to describe the evolution of the perturbations.

For the sake of convenience, we define the comoving distance to an object $\bm{r}$ as:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{ComovingCoor}
{ \bm{r} \equiv \frac{\bm{x}}{a}, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\bm{x}$ is the proper distance and $a$ is the time-dependent scale factor. In this new coordinate system, the expansion of the Universe is factored out, and therefore, the comoving distance between two fixed comoving points is always constant. This definition has an interesting consequence when the proper velocity $\bm{u}$ is calculated:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\begin{eqnarray}
\nonumber
\bm{u} \equiv \frac{d\bm{x}}{dt} &=& \dot a \bm{r} + a\frac{d\bm{r}}{dt} \\
\label{eq:ProperVel}
&=& H\bm{x} + \bm{v}
\end{eqnarray}
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
with
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{ComovingVel}
{\bm{v} \equiv a\frac{d\bm{r}}{dt}.}
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

The second term in the right-hand side in equation~(\ref{eq:ProperVel}) is dubbed peculiar velocity and refers to the velocity of an object relative to the rest frame in which the expansion is isotropic and homogeneous. An object that is static in this frame, i.e. $\bm{v} = 0$, has still a non-vanishing proper velocity as the first term prevails. That term represents the recessional velocity that is caused solely by the expansion of the Universe. At the present-day Universe, this corresponds to Hubble's law, which was presented before in equation~(\ref{HubblesLaw}).

Previously, we have approximated the behaviour of the matter as an ideal gas. Under this assumption, the classical equations of fluid dynamics along with the equation of state of an ideal gas can be employed to describe the evolution of the fluctuations \citep{Longair2008}:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\begin{eqnarray}
\label{eq:ContinuityEquationC}
\matrix{\mbox{\footnotesize{Continuity}} \cr \mbox{\footnotesize{equation}}} & &
\der{\delta}{t} = - \frac{1}{a}\nabla_r \cdot \cor{ (1+\delta)\bds v },\\
\nonumber{}
\\
\label{eq:EulerEquationC}
\matrix{\mbox{\footnotesize{Euler's}} \cr \mbox{\footnotesize{equation}}} & &
\der{\bds v}{t} + \frac{\dot a}{a}\bds v + 
\frac{1}{a}\pr{ \bds v\cdot \nabla_r }\bds v = 
-\frac{\nabla_r P}{a \bar \rho(1+\delta)} - 
\frac{1}{a}\nabla_r \Phi, \\
\nonumber{}
\\
\label{eq:PoissonEquationC}
\matrix{\mbox{\footnotesize{Poisson's}} \cr \mbox{\footnotesize{equation}}} & &
\nabla^2_r \Phi = 4\pi G\bar \rho a^2 \delta, \\
\nonumber{}
\\
\label{eq:EquationState}
\matrix{\mbox{\footnotesize{Equation}} \cr \mbox{\footnotesize{of state}}} & &
\nabla_r P = c_s^2\bar \rho \Delta \delta + \frac{2}{3}\bar{T}\rho\Delta s,
\end{eqnarray}
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\delta$ is the density parameter, defined by $\rho = \bar \rho + \delta \rho = \bar \rho (1+\delta)$; $\bm{v}$ is the peculiar velocity; $P$ is the gas pressure; $\Phi$ is the peculiar gravitational potential; $c_s$ is the speed of the sound; $\bar T$ is the mean temperature of the gas; and $s$ is the specific entropy. Note that all the equations are expressed in comoving coordinates, as in this way, the expansion of the space-time is factored out and the treatment of the fluctuations is purely Newtonian.

We combine the previous set of equations to obtain the following differential equation for the density parameter:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{EquationPerturbations}
{ \der{^2 \delta}{t^2} + 2\frac{\dot a}{a} \der{\delta}{t} = 
4\pi G \bar \rho \delta + \frac{c_s^2}{2}\nabla^2 \delta +
\frac{2}{3}\frac{\overline T}{a^2}\nabla^2 s. }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

In the linear regime, the density field can be decomposed into a set of orthogonal modes. For the case of a flat universe, i.e. $k=0$, the orthogonal base corresponds to sinusoidal functions. The density parameter can be therefore expressed in the following form:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{EquationPerturbations}
{ \delta( \bm{r}, t ) = \int \delta_{\bm{k}} e^{\bm{i}\bm{k}\cdot \bm{r}}d^3\bm{k}, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\delta_{\bm k}$ is the amplitude of an orthogonal mode and represents the Fourier transform of the density field. The equation that describes the evolution of $\delta_{\bm k}$ is then given by:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{EquationPerturbations_Kspace}
{ \dtot{^2 \delta_{\bds k}}{t^2} + 2\frac{\dot a}{a}\dtot{\delta_{\bds k}}{t} =
\cor{ 4\pi G \bar \rho - \frac{c_s^2}{a^2}k^2 }\delta_{\bds k}. }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

By inserting the Friedmann equations~(\ref{eq:FriedmannEq}), the previous equation can be solved for each mode $\delta_{\bds k}(t)$ of the density field. Initial conditions are provided by the power spectrum, which describes the distribution of power into each mode of the field. An initial power spectrum for matter fluctuations that is motivated by inflation theories is \citep{Guth1981,Linde1982}:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{PowerSpectrum}
{ P(k) \propto k^{n_s-1}, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $n_s$ is the spectral index and one of the cosmological parameters. Its measured value is $n_s\sim 1$ \citep{Planck2018}, which implies a flat power spectrum.

An analogous procedure can be followed for fluctuations in the radiation field and for relativistic matter. We refer the reader to the books of \citet{padmanabhan1995} and \citet{Longair2008} for exhaustive details and derivations.

%==================================================================================================
\section{Zeldovich Approximation}
\label{sec:ZeldovichApproximation}
%==================================================================================================

When matter perturbations grow much larger than the background density, the stronger modes of the density field start to couple with each other as more complex interactions come to play, breaking thus the assumption of linearity and giving rise to the non-linear regime. At the onset of the non-linear regime, when perturbations do not deviate significantly from the linear solution, it is possible to describe the evolution of the perturbations in terms of the displacement field $\bds \Psi$, defined by the following expression:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{eq:ZeldovichTrayectory}
{ \bds r_f( t,\bds q ) = a(t)\bds r = a(t)\cor{ \bds q + \bds \Psi(\bds q,t) }, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\bds r_f$ is the Lagrangian trajectory of a portion of fluid, $\bds r$ is its comoving position at the time $t$, and $\bds q$ corresponds to the initial Lagrangian coordinate of the fluid element when the medium is unperturbed. The position and velocity fields of the perturbations can be solved in terms of $\bds \Psi$. This is the so-called Zeldovich approximation \citep{Zeldovich1970}.

From equation~(\ref{EquationPerturbations}) for the evolution of the density parameter, it is possible to demonstrate that the displacement field satisfies \citep{Yoshisato2006}:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{eq:Displacement}
{ \der{^2  \bds \Psi}{t^2} + 2H\der{\bds \Psi}{t} =
 \frac{ 3}{2}H^2 \bds \Psi,  }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{eq:DisplacementForm}
{ \bds \Psi = \frac{3}{2}H_0^{-2}a(t)\nabla \Phi,  }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\Phi$ is the peculiar gravitational potential, which can be solved by integrating Poisson's equation~(\ref{eq:PoissonEquationC}). To this end, it is convenient to express the principle of mass conservation in terms of the initial Lagrangian coordinates:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{eq:MassConservation}
{ \rho(\bds r, t)d^3 \bds r = \bar{\rho}(t)d^3 \bds q.  }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
From the the Jacobian $\partial q_i / \partial r_j$ of the transformation $\bds r \rightarrow \bds q$, the perturbed density field can be rewritten as \citep{padmanabhan1995}:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{eq:PerturbedFieldDensity}
{ \rho(\bds r, t) = \frac{\bar \rho (t)}{\pr{ 1 - a(t) \lambda_1(\bds q)}
\pr{ 1 - a(t) \lambda_2(\bds q)}\pr{ 1 - a(t) \lambda_3(\bds q)} }, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $-\lambda_i(\bds q)$ are the eigenvalues of the Jacobian and are sorted such that $\lambda_1\geq\lambda_2\geq\lambda_3$. These eigenvalues can be interpreted in a geometrical way, i.e., as indicators of the local collapse or expansion of the fluid into the direction of the corresponding eigenvector; thus, for example, if $\lambda_i > 0$, the fluid is collapsing locally into the direction of the eigenvector $\bds u_i$, whereas if $\lambda_i < 0$, it is expanding into the same direction. The Zeldovich approximation is widely used in cosmological simulations because it allows to construct the position and velocity fields of the perturbations, which serve as initial conditions in particle-based numerical techniques to solve the problem of structure formation in the Universe.