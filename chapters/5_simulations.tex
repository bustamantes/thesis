%##################################################################################################
\chapter{Simulations of galaxy formation}
\label{cha:5_simulations}
%##################################################################################################

The early evolution of perturbations in the matter content is remarkably well described by the linear regime formalism (see chapter~\ref{cha:1_structure_formation}). Nevertheless, after the recombination epoch at redshifts $z\lesssim1100$, once the primordial gas settles at the centre of dark matter haloes, non-liner gravitational evolution and baryonic effects couple the small-scale modes of the density field, thereby breaking the assumption of linear independence and rendering it infeasible to follow the processes of evolution and hierarchical assembly of galaxies in an analytical fashion. Numerical approaches have to be adopted instead as they provide an effective way to solve complex non-linear problems. In this context, computer simulations have become in the last decades an essential tool to understand the problem of structure formation.

In order to realistically reproduce the present-day Universe, state-of-the-art simulations of galaxy formation incorporate a large variety of physical processes that operate at a wide range of scales and are intricately coupled to each other. The latest measurements of the cosmological parameters of the $\Lambda$CDM model show that matter is only the second most important component in the Universe after dark energy \citep{Planck2018}. Gravity is the main driver of structure evolution at the largest scales, and thus, it must be the basic ingredient of any simulation of galaxy formation. To this end, particle-based methods that discretise the matter content are commonly used to solve gravity at all scales. The measured values of the density parameters $\Omega_m = 0.3089$ and $\Omega_b = 0.0486$ show that around $15\%$ of the matter content corresponds to baryons, which have a collisional nature. This implies that the evolution of baryons is not only governed by gravity but also by the laws of magnetohydrodynamics. Owing to this, different particle-based and mesh-based magnetohydrodynamical schemes are implemented in simulations to solve the baryonic component. Finally, we have seen in the last chapters how small-scale baryonic processes such as gas cooling, star formation, stellar and AGN feedback play a key role in galaxy formation and evolution. In spite of this, these processes cannot be fully resolved in a cosmological setup due to the associated unaffordable computational cost. Instead, coarse-grained sub-grid approaches are often used to approximate this physics at the price of higher systematic uncertainties.

In this chapter, we present the simulations used in this thesis as well as the different models of gravity, magnetohydrodynamics, gas cooling, star formation, stellar feedback and chemical evolution.  Note that a section on black hole physics is omitted in this chapter, because the numerical implementation of black hole physics will be discussed in detail in chapter~\ref{cha:8_spinevolution}, section~\ref{SecBHmodel}.

%==================================================================================================
\section{Gravitational forces}
\label{sec:Gravity}
%==================================================================================================

Given the unknown nature of dark matter, coarse-grained discretisation schemes are commonly employed in simulations to represent the dark matter density field as a collection of particles that interact gravitationally with each other, but ignoring in most cases possible self-interaction effects. The dynamics of collisionless particles that interact via long-range forces (e.g. gravity) can be described by the Vlasov equation:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\begin{eqnarray}
\nonumber
\frac{df}{dt} &=& \frac{\partial f}{\partial t} + \frac{dx_i}{dt}\frac{\partial f}{\partial x_i} + \frac{dv_j}{dt}\frac{\partial f}{\partial v_j}  = 0\\
  \label{eq:Vlasov}
&=& \frac{\partial f}{\partial t} + v_i\frac{\partial f}{\partial x_i} + \frac{d\phi}{dx_j}\frac{\partial f}{\partial v_j}  = 0,
\end{eqnarray}
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $f$ is the phase-space distribution function and $\phi$ is the gravitational potential. The Poisson equation links the potential with the density field $\rho$. In terms of the distribution function, they are expressed as:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{DensityVlasov}
{ \rho = m \int f d^3 x, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{PoissonVlasov}
{ \nabla^2 \phi = 4\pi G(\rho - \bar \rho), }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $m$ is the mass of a simulation particle and $\bar \rho$ the mean density of the Universe. Thus, in order to compute the gravitational interaction of collisionless particles in simulations, it is necessary to solve this system of equations. 

The most straightforward of the solutions consists in summing up the monopole contribution of each particle at a given point. This is the so-called direct summation method and is only suitable for problems with a small number of particles (few-body problem) due to its poor CPU-time scalability, i.e. $O(N^2)$, with $N$ the number of particles. For cosmological setups with about $10^8-10^{10}$ particles, such a method is computationally unaffordable and other methods have to be implemented instead. For instance, a much faster approximation is the oct-tree method \citep{Barnes86}, in which the simulation volume is hierarchically divided into branches (octants) until each one contains a predetermined number of particles. In Figure~\ref{fig:TreeConstruction}, we illustrate the construction of the tree structure for a random set of particles. The gravitational interaction at a given point is calculated through direct summation, multipole expansion, and particle mesh techniques \citep[see e.g.][]{Dawson1983}, in order of hierarchy in the tree \citep[for an implementation, see e.g.][]{Springel2005G}. The computing time required to build the tree structure scales with the number of particles as $O(N \log(N))$. Another family of methods consist in solving the Poisson equation directly on a grid with Fourier based techniques \citep[see e.g.][]{Guillet2011}, or iterative relaxation methods such as the multigrid approach.

%.........................................................................
%	Figure 1
%.........................................................................
%Tree code
\begin{figure*}
\centering
\includegraphics[width=0.6\textwidth]
{./figures/5_simulations/TreeCode.png}
\caption{\small{Illustration of the construction of a hierarchical oct-tree. The gravitational interaction between simulation particles is computed through direct summation, multipolar expansion or particle mesh techniques according to the level in the tree.}}
\label{fig:TreeConstruction}
\end{figure*}
%.........................................................................

%==================================================================================================
\section{Gas magneto-hydrodynamics}
\label{sec:Gas_Hydro}
%==================================================================================================

The macroscopic dynamics of the baryonic content of the Universe is governed by the laws of gravity and ideal magnetohydrodynamics (MHD). The equations of ideal MHD are given by:

%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{MHD_1}
{ \frac{\partial \rho}{\partial t} + \nabla_{\bds r}\cdot (\rho \bds{v}) = 0, }
\eq{MHD_2}
{ \frac{\partial \rho\bds v}{\partial t} + \nabla_{\bds r}\cdot \left( \rho \bds{v} \bds{v}^T + p_{\mathrm{tot}} - \bds B \bds B ^T \right) = 0, }
\eq{MHD_3}
{ \frac{\partial E}{\partial t} + \nabla_{\bds r}\cdot \left[ \bds v ( E + p_{\mathrm{tot}} ) - \bds B (\bds v \cdot \bds B) \right] = 0, }
\eq{MHD_4}
{ \frac{\partial \bds B}{\partial t} + \nabla_{\bds r}\cdot \left( \bds{B}\bds{v}^T -  \bds{v}\bds B ^T \right) = 0, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\bds r$ is the physical position; $\rho$, $\bds v$ and $\bds B$ represent the local gas density, velocity field and magnetic field strength, respectively. The total gas pressure and total energy per unit volume are defined as $p_{\mathrm{tot}} = p_{\mathrm{gas}} + \frac{1}{2}\bds B^2$ and $E = \rho u_{\mathrm{th}} + \frac{1}{2}\rho \bds v^2 + \frac{1}{2}\bds B^2$, with $u_{\mathrm{th}}$ denoting the thermal energy per unit volume \citep[see e.g.][]{Pakmor2013}.

The inherent complexity in solving the previous set of non-linear equations demands the implementation of numerical schemes commonly referred to as hydro-solvers. Traditionally, two different families of hydro-solvers have been used in the context of simulations of galaxy formation, both in isolated and cosmological setups. The first family is the so-called Smoothed Particle Hydrodynamics (SPH), a set of Lagrangian schemes based on moving point masses that represent parcels of the fluid \citep{Monaghan1992} (for an implementation, see e.g. the GADGET code by \citet{Springel2005G}); the second family consists of Eulerian solvers on an adaptive stationary mesh known as Adaptive Mesh Refinement (AMR) \citep{Berger1989} (for an implementation,  see e.g. the RAMSES code by \citet{Teyssier2002}).  

Owing to the Lagrangian nature of SPH schemes, simulation particles of fixed mass travel by construction along with the fluid. As a consequence, regions of high-density are naturally sampled with a larger number of particles than low-density regions. This feature provides a self-adjusting spatial resolution that allows a rather straightforward computational implementation. Additionally, sub-resolution physics models can be easily incorporated as each simulation particle represents a fixed fluid parcel, and thus, advection terms are automatically accounted for. Nevertheless, SPH schemes have also been shown to spuriously suppress fluid instabilities due to the smoothing and interpolation procedure applied to recover the continuous fluid quantities. Also, shocks, which are crucial in the process of gas accretion into galaxies, are commonly broadened in SPH schemes and require artificial viscosity. In AMR schemes, the fluid equations are directly solved on a fixed mesh that spans the simulation volume and whose resolution can be locally (de)refined. Therefore, no interpolation is required to recover the fluid quantities and shock dynamics and fluid instabilities are consequently more accurately captured. However, the stationary nature of the grid and its fixed geometry create problems with Galilean invariance and the conservation of angular momentum. For a detailed  discussion and comparison of SPH and AMR see \citet{Plewa2001}).

Recently, a completely new approach to solve hydrodynamical problems was introduced by \citet{Springel2010} and implemented in the AREPO code. It combines the strengths of AMR and SPH but overcomes many of their weaknesses. This is achieved by solving the fluid equations on an unstructured mesh based on a Voronoi tessellation defined over a set of mesh-generating points that move with the local fluid velocity. The geometry of the mesh resembles very closely that of the point distribution, retaining thus the automatic adaptivity inherent of SPH and also keeping a grid to capture shocks like AMR does. We note that this approach is only approximately Lagrangian as there exist small advection terms across the interfaces of the Voronoi cells, and thus, a volume element represents a fixed parcel of the fluid only during a finite time interval. The previous features make AREPO highly efficient and accurate for simulating a wide range of hydrodynamical problems, making it on of the best available approaches for computing galaxy evolution in the gaseous cosmic web of the Universe.

We show in Figure~\ref{fig:HydroSolvers} three snapshots of the density field of the Kelvin-Helmholtz instability problem solved with the three different hydro-solvers. For illustrative purposes, the geometry of the underlying mesh is highlighted for AMR and AREPO. For SPH, the density field is reconstructed through a smoothing kernel around each fluid particle.

%.........................................................................
%	Figure 2
%.........................................................................
%Hydro solvers
\begin{figure*}
\centering
\includegraphics[width=1.0\textwidth]
{./figures/5_simulations/HydroSolvers.png}
\caption{\small{Density field of the Kelvin-Helmholtz instability solved with 3 different hydro-solvers. SPH (left), AMR (centre) and the AREPO hydro-solver (right). Image credits: \citep{Price2008}, \citep{Schive2010} and \citep{Springel2010}, respectively.}}
\label{fig:HydroSolvers}
\end{figure*}
%.........................................................................

%==================================================================================================
\section{Radiative cooling processes}
\label{sec:Cooling}
%==================================================================================================

In an ideal, adiabatic and magnetised gas, the evolution of the total energy density is described by equation~\ref{MHD_3}. Nevertheless, after the recombination epoch, once baryons decouple from the photon field and collapse into dark matter haloes, non-adiabatic cooling and heating mechanisms such as radiative processes, shock heating, photoionisation from local sources and an ultraviolet background become important. As a consequence, equation~\ref{MHD_3} needs to be modified in order to include source and sink terms, resulting in:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{CoolingEnergy}
{ \frac{\partial E}{\partial t} + \nabla_{\bds r}\cdot \left[ \bds v ( E + p_{\mathrm{tot}} ) - \bds B (\bds v \cdot \bds B) \right] = -\Lambda_{\mathrm{net}}(\rho, E, \{X\}), }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\Lambda_{\mathrm{net}}(\rho, E, \{X\})$ is the cooling function that describes external sinks or sources of heat for the gas. This function generally depends on the the local density, energy (temperature), chemical species present in the gas and external factor such as radiation fields.

Characteristic timescales associated to cooling processes can vary by orders of magnitude from one region to another depending on the local state of the gas. Because of this, the implementation of explicit time integration schemes to solve cooling processes in simulations has proven to be very challenging due to the extremely small time steps that have to be resolved in order to reliably follow the evolution of the gas. A common solution to this consists in using an implicit integration scheme in which adiabatic terms are computed first and then the following implicit equation is solved:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{ImplicitCoolingTerms}
{ \hat{E}_i^{(n+1)} = E_i^{(n)} + \dot E^{\mathrm{ad}}\Delta t - \Lambda_{\mathrm{net}}\left[ \rho_i^{(n)}, \hat{E}_i^{(n+1)}, \{X\} \right]\Delta t, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\dot E^{\mathrm{ad}}\Delta t$ is the adiabatic contribution, i.e. the solution to the homogeneous equation~\ref{MHD_3}, $i$ denotes the $i$-th volume element and $n$ the $n$-th time step. The corresponding rate of change of the energy density is finally given by:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{EnergyEvolutionCooling}
{ \dot{E}_i = \frac{\hat E_i^{(n+1)} - E_i^{(n)} }{\Delta t}. }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
In order to prevent numerical issues such as overshooting, which might result in very small or even negative energies, often each resolution element is allowed to lose only half of its energy at a given time step \citep{Springel2001}.

The explicit form of the cooling function $\Lambda_{\mathrm{net}}(\rho, E, \{X\})$ is a superposition of several non-adiabatic cooling and heating (treated as negative cooling) terms. Cooling processes consist of primordial cooling \citep{Cen1992, Sutherland1993, Katz1996}, metal-line and Compton cooling \citep{Wiersma2009} and molecular cooling \citep{Glover2008}, each of which operates in different regimes of the gas. Heating terms include ionising UV radiation from quasars and early star formation \citep{Haardt2012, FaucherGiguere2009}, which is usually implemented as a homogeneous background radiation field that affects the gas.

%==================================================================================================
\section{Star formation and stellar winds}
\label{sec:Star_Formation}
%==================================================================================================

Once the primordial baryons settle in the centre of dark matter haloes, radiative cooling triggers a run away process in which the gas undergoes small-scale fragmentation and gravitational collapse, becoming thus denser and more prone to further cooling. This ultimately results in dense, compact and cold gas clouds that constitute the perfect environment for the formation of stars. The exact physical mechanisms that shape the process of star formation are still a major research topic \citep{MacLow2004}. In addition to these theoretical uncertainties, computational limitations arise from the minimum mass resolution that can be achieved in simulations for the gas component (around $10^6\, \mathrm{M}_{\odot}$ in modern cosmological simulation). This implies that the formation of individual stars cannot be self-consistently followed and sub-resolution effective approaches have to be adopted instead.

One of the most widely used approaches to model the ISM and the star formation process in simulations is the self-regulated two-phase model from \citet{Springel2003}. In this model, a resolution element (gas particle or cell\footnote{This ambiguity when referring to a resolution elements is kept as most sub-grid models can be implemented in particle based as well as in grid based hydro-solvers. \\}) of the ISM gas that reaches a density threshold $\rho_{\mathrm{th}}$ is split in two non-resolved co-existing phases, namely: an ambient volume-filling hot phase, and a cold phase condensed in clouds from which stars can form, both in pressure equilibrium with each other. Several internal processes govern the mass losses and the mass exchange between the two phases, e.g. star formation, supernovae and cloud evaporation remove mass from the cold phase, whereas thermal instabilities driven by radiative cooling deposit mass from the ambient hot phase into the cold phase. The corresponding evolution equations for the densities are given by:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\begin{eqnarray}
\label{eq:coldphase}
  \frac{d \rho_c}{d t} &=& -\frac{\rho_c}{t_{\star}} - A\beta \frac{\rho_c}{t_{\star}} + \frac{1 - f}{u_h - u_c} \Lambda_{\mathrm{net}}(\rho_h, u_h), \\
\label{eq:hotphase}
  \frac{d \rho_h}{d t} &=& \beta \frac{\rho_c}{t_{\star}} - A\beta \frac{\rho_c}{t_{\star}} - \frac{1 - f}{u_h - u_c} \Lambda_{\mathrm{net}}(\rho_h, u_h),
\end{eqnarray}
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\rho_c$ and $\rho_h$ denote the density of the cold and hot phases, respectively.

The first term of equation~\ref{eq:coldphase} corresponds to the rate at which the cold gas reservoir is converted into stars, which occurs at a characteristic timescale $t_{\star}$, defined as $t_{\star}\equiv m_g / \dot m_{\star}$, where $m_g$ is the mass of the gas and $\dot m_{\star}$ the star formation rate. The second term corresponds to the rate at which cold clouds are evaporated by nearby supernovae. The efficiency of this process is denoted by $A$ and is usually assumed to be a function of the gas density, i.e. $A \propto \rho^{-4/5}$ \citep{McKee1977}. The parameter $\beta$ quantifies the fraction of stars that are very massive ($M>8\, \mathrm{M}_{\odot}$) and die out quickly as supernovae (instantly for computational purposes). The third term corresponds to the rate at which hot gas cools through thermal instabilities. The second and third terms also appear in equation~\ref{eq:hotphase} but with opposite sign, which reflects the mass exchange between the two phases. In contrast, the first term does not appear as the transformed cold gas stays locked up in the stellar phase. The first term of equation~\ref{eq:hotphase} corresponds to the gas deposited from the stellar phase into the ambient phase by supernovae of massive stars. Note that only gas in the hot phase is assumed to experience radiative cooling.

In the model of \citet{Springel2003}, the characteristic timescale of star formation is set to be a function of the density $\rho$:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{CoolingTime}
{ t_\star(\rho) = t_0^\star\left( \frac{\rho}{\rho_{\mathrm{th}}} \right)^{-1/2}, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $t_0^\star$ is the a free parameter that is calibrated to reproduce the Kennicutt-Schmidt law \citep{Kennicutt1998}, an observational relation that links the total gas surface density $\Sigma_{\mathrm{gas}}$ with the star formation surface density $\Sigma_{\mathrm{SFR}}$. The equation that balances the rate at which gas is locked up in the stellar phase $\dot m_{\star}$, mass losses from the cold gas phase $\dot m_g$, and the rate at which hot gas is ejected in the form of stellar winds by core collapse (SNII) supernovae and stars in the asymptotic giant branch (AGB) phase $\dot m_w$ is given by
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{BalanceEquation}
{ \dot m_{\star}(1 + \eta_w) = -\dot m_g, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\eta_w = \dot m_w/\dot m_{\star}$ is the mass loading factor. Combining this with equation~\ref{CoolingTime} and integrating yields:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{WindsStarsSolution}
{ \dot m_{\star,w} = m_g\left(1-e^{-(1+\eta_w)\Delta t/t_{\star}}\right). }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

Accounting for star formation by directly solving this balance equation for each gas particle/cell poses the problem that the related processes are occurring at sub-resolution scales. While the hot and cold phases can coexist because they are both collisional components, keeping track of an extra collisionless stellar phase within the same resolution element is technically unfeasible. An elegant alternative to overcome this issue is to adopt a probabilistic approach instead \citep{Springel2003}. The combined probability for star formation and wind launching can be defined as:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{WindsStarsProbability}
{ P_{\star,w}(t_\star) \equiv 1-e^{-(1+\eta_w)\Delta t/t_{\star}}. }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
Thus, a gas particle/cell will be completely converted into a stellar/wind particle with a probability $P_{\star,w}$, keeping in this way the stellar and the gas components separated. While this is a rather imprecise and rough approximation at small scales, it is numerically robust and satisfies the balance equation~\ref{BalanceEquation} at macroscopic scales. Gas particles/cells that are not converted and have a density above the threshold value $\rho_{\mathrm{th}}$ are still evolved with equations~\ref{eq:coldphase} and \ref{eq:hotphase}. On the other hand, converted gas particles/cells can become either stellar particles or wind particles, with probabilities $1/(1+\eta_w)$ and $\eta_w/(1+\eta_w)$, respectively. Stellar particles inherit the properties of their parent gas particles/cells, including position, mass, momentum and metallicity, are treated as a collisionless component and represent a single stellar population (SSP) with a given initial mass function (IMF), usually a \citet{Salpeter1955} or a \citet{Chabrier-2003} IMF. On the other hand, wind particles represent a parcel of ejected gas and thus, they still have a collisional nature. They also inherit the properties of the parent gas particle/cell and are additionally launched with a velocity that scales with the local one-dimensional dark matter velocity dispersion \citep{Okamoto2010}. In order to simulate the launching and ejection process, they are temporarily decoupled from the hydrodynamic interactions until they reach a low density region with density $\rho<0.05\rho_{\mathrm{th}}$, or a maximum travel time is exceeded (typically $2.5\%$ of the Hubble time at the corresponding redshift). After one of these conditions is met, they are recoupled and either become fluid particles in particle based hydro-solvers, or deposit all their mass together with metals, thermal energy and momentum into the nearest gas cell in mesh based hydro-solvers.

%==================================================================================================
\section{Chemical evolution}
\label{sec:Chemical}
%==================================================================================================

In addition to building up the stellar content in galaxies, the process of star formation also drives the enrichment of metals in the gas component. Together with primordial nucleo\-synthesis, stellar processes such as SNII and SNIa supernovae, and winds from AGB stars constitute the primary mechanisms to produce and distribute metals in the Universe.

As discussed before, the massive stellar precursors of SNII supernovae are assumed to die out instantly, which together with the fraction of AGB stars in each SSP, result in the probabilistic generation of wind particles. These particles carry away the mass of the expelled gas and are loaded with heavy elements according to SNII \citep[see e.g.][]{Portinari-1998} and AGB \citep[see e.g.][]{Karakas-2010} yields. The enriched material is deposited into the gas once the wind particles are recoupled into the hydrodynamics. On the other hand, the stellar precursors of SNIa supernovae live longer and thus, the number of SNIa events has to be calculated by integrating the delay time distribution $g(t)$:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{DelayIntegral}
{ N_{\mathrm{Ia}}(t, \Delta t) = \int_{t}^{t+\Delta t} g(t' - t_0)dt', }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{DelayDF}
{ g(t)=\left\{
  \begin{array}{rr}
    0\hspace{3.2cm} \mathrm{if}\ t<\tau_{8\mathrm{M}_\odot},\\
    N_0\left( \frac{t}{\tau_{8\mathrm{M}_\odot}} \right)^{-s}\frac{s-1}{\tau_{8\mathrm{M}_\odot}}\ \ \ \mathrm{if}\ t>\tau_{8\mathrm{M}_\odot},
  \end{array}
\right. }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $N_0$ is a normalisation parameter typically set as twice the supernovae rate. $s=1.12$ and $\tau_{8\mathrm{M}_\odot} = 40\, \mathrm{Myr}$ are the power law index and the main sequence lifetime of a $8\, \mathrm{M}_{\odot}$ star, which is the upper mass limit for SNIa. The corresponding amount of mass and metals that are returned to the ISM are calculated from SNIa yield tables \citep[see e.g.][]{Thielemann-2003, Travaglio-2004}, and distributed among neighbouring gas particles/cells for each stellar particle.

%==================================================================================================
\section{Simulations}
\label{sec:Simulations}
%==================================================================================================

All the simulations used throughout this thesis were carried out with the code AREPO and make use of the previously described techniques and sub-grid models, which are also integrated in the code. In chapter~\ref{cha:6_metallicity_dilution_sim}, we use the set of simulated Milk-Way-like galaxies of the Auriga project \citep{Grand-2017}. In chapter~\ref{cha:8_spinevolution}, we run cosmological simulations that employ the physics model of the IllustrisTNG project \citep{Pillepich2018, Springel2018}. A complete description of these simulations is presented in the respective chapters.
