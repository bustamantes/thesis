%##################################################################################################
\chapter{Properties of galaxies}
\label{cha:2_properties_galaxies}
%##################################################################################################

Although galaxies are the fundamental units of structure in the Universe, they are complex and rich systems that harbour many different physical phenomena. As mentioned in chapter~\ref{cha:0_introduction}, dark matter perturbations can grow before recombination due to the absence of radiative pressure, allowing much higher density anisotropies of the dark matter component relative to that of the baryons, and giving thus rise to the dark matter haloes. These haloes provide the potential wells into which primordial gas collapses after recombination, forming thus the first galaxies. Through radiative cooling, the gas can further collapse and form the first population of stars, which later on deposit heavy elements into the ISM via supernovae outflows and stellar winds. New stellar populations are born and the cycle is repeated, resulting in an overall evolution of the galaxy properties. In this chapter, we present some important properties and relations of galaxy populations, namely: properties of the dark matter haloes, the bimodality in galaxy structure and colour, the main sequence of star-forming galaxies and the mass-metallicity relation.

%==================================================================================================
\section{Dark matter haloes}
\label{sec:DMH}
%==================================================================================================

Perturbations of the dark matter field that reach and exceed a certain threshold value in the resulting overdensity relative to the background density undergo a spherical collapse into virialised structures known as dark matter haloes. They are characterised by the virial radius $r_{200}$, the radius of a region in which the average density is $200$ times the cosmic background density; and the virial mass $M_{\mathrm{vir}}$, which is the total mass enclosed within $r_{200}$. Additionally, dark matter haloes acquire angular momentum through tidal torques, which endows them with centrifugal support \citep{Hoyle1951, Peebles1969, White1984} and shapes their inner structure. The angular momentum is quantified by the dimensionless spin parameter $\lambda$, which indicates the specific angular momentum of the halo normalised by the theoretical maximum. 

The radial structure of dark matter haloes has been found to be universal and independent of mass \citep{Navarro1997}, and is given by:
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
\eq{eq:NFW}
{ \rho(r) = \frac{\rho_0}{(r/r_s)(1+r/r_s)^2}, }
%eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
where $\rho_0$ is the density at the centre of the halo; and $r_s$ is the scale radius, which is related to the virial radius via $r_s = r_{200}/c$, where $c$ is the concentration parameter.

Once dark matter halos have formed, they provide the potential wells into which primor\-dial gas gravitationally collapses through the filamentary cosmic web structure. Infalling gas is shock-heated as it encounters a settled medium inside the halo, thereby reaching high virial temperatures $T \sim 10^6\, \mathrm{K}$ \citep{Rees1977}. After this point, radiative processes drive the cooling of the gas, which causes it to segregate from the dark matter and subsequently settle down into a centrifugally supported disc structure \citep{Fall1980, Mo1998}, giving thus rise to the ISM and enabling the star formation processes \citep{White1978}. The details of gas accretion and cooling are strongly dependent on the properties of the dark matter haloes. For instance, small haloes with masses $M_{\mathrm{vir}}\leq 10^{12}\, \mathrm{M}_{\odot}$ also accrete gas via cold streams along the filaments of the cosmic web. In this scenario, cold gas can penetrate deep into the halo and avoids being shock-heated. For more massive haloes, the cold accretion mode is suppressed by shock-heating and hot accretion becomes dominant, causing radiative cooling to play a critical role in the gas settling \citep{Silk1977, Rees1977, Keres2005, Dekel2006}. Galaxy properties are therefore expected to correlate with the properties of the dark matter haloes \citep{Mo1998}: 

In addition to the above mentioned processes of gas accretion and cooling, and the formation of the disc structure, there are other crucial and complex processes that have an important influence on galaxy evolution. For example, the feedback from star formation can balance and overcome the cooling process by injecting heat into the galactic and extra-galactic gas via stellar winds and supernovae driven outflows. The feedback from supermassive black holes that form at the centre of galaxies can drive very powerful gas outflows if there is a gas reservoir that can sustain a continuous accretion \citep{LyndenBell1969, McCarthy2007, Puchwein2008}. Feedback processes result in most cases in a shutting down of star formation activity by either expelling or heating up the gas, thereby impacting directly the stellar and the gas distributions of galaxies. Moreover, internal secular processes and galaxy mergers can significantly redistribute the baryon content inside galaxies, leading thus to a large variety of galaxy morphologies.

Thus, in order to understand the build-up history of galaxies in the context of galaxy formation, it is important to determine the baryonic structure and the processes that shape it.

%==================================================================================================
\section{Galaxy bimodality}
\label{sec:GalaxyBimodality}
%==================================================================================================

%.........................................................................
%	Figure 1
%.........................................................................
%Hubble Sequence
\begin{figure*}
\centering
\includegraphics[width=0.7\textwidth]
{./figures/2_properties_galaxies/TurningFork.png}
\caption{\small{The Hubble classification scheme of galaxies. Image credits: \footnotesize{\url{https://en.wikipedia.org/wiki/Hubble_sequence\#/media/File:HubbleTuningFork.jpg}.}}}
\label{fig:HubbleSequence}
\end{figure*}
%.........................................................................

%.........................................................................
%	Figure 2
%.........................................................................
%Colour Bimodality
\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]
{./figures/2_properties_galaxies/Colour_Bimodality.pdf}
\caption{\small{Colour-magnitude distribution of local galaxies from the SDSS ($z<0.1$). The bimodality of the distribution is reflected in the two peaks in galaxy colour, which are associated to the red and blue sequences (shaded regions) \citep{Baldry2004}. Image credits: \footnotesize{\url{http://www.astro.ljmu.ac.uk/~ikb/bimodal-figure/}}.}}
\label{fig:Colour_Bimodality}
\end{figure}
%.........................................................................

Observations of local galaxies have revealed a clear bimodality in several galaxy properties. This was first evinced by \citet{Hubble1926} in the form of a bimodality in the morphology of the population of local galaxies. In this pioneering work, Hubble devised a classification scheme in which galaxies are separated into ellipticals (so-called early-type galaxies, ETGs) and into spirals (so-called late-type galaxies, LTGs). They can be seen on the right and left parts of the diagram shown in Figure~\ref{fig:HubbleSequence}, respectively.

Spiral galaxies are systems in which most of the stars and the gas move along regular circular orbits and are, therefore, concentrated in a geometrically flat disc. Galactic discs can in turn develop features like arms and bars, which are used in the Hubble diagram to subdivide spiral galaxies into different groups. For instance, the presence/absence of bars determines whether a galaxy is a \emph{SB} (barred spiral) or a \emph{S} (spiral) system. Likewise, the number of spiral arms and their pitch angle determine the subcategory, which is indicated by a lowercase letter: \emph{a}, \emph{b} or \emph{c}. Thus, for example, \emph{SBa} refers to a barred spiral galaxy with tightly wound arms. 

On the other hand, elliptical galaxies are more spheroidal systems with a smoother appearance. The orbits of gas and stars are not regular and instead are dominated by random motion, implying thus a limited rotational support. In the Hubble diagram, they are denoted by the letter \emph{E}, followed by a number that represents the ellipticity of the system, e.g. $E0$ correspond to a fully spherical configuration, whereas $E7$ to a very flat spheroid. A somewhat intermediate category is represented by lenticular galaxies, which are disc galaxies with no spiral arms and are denoted by $S0$ in the Hubble diagram.

Recent studies in local galaxy populations have revealed a connection between the morphology bimodality and a colour bimodality, which is in turn linked to the star formation properties of galaxies \citep{Strateva2001, Kauffmann2003, Baldry2004}. Spiral galaxies exhibit substantial ongoing star formation activity due to the high amount of gas that acts as fuel \citep{Kennicutt1998}. As a result, short-lived massive and hot stars that emit light at short optical wavelengths are produced in high abundance and spiral galaxies display blue optical colours. In contrast, stellar populations in elliptical galaxies are often dominated by long-lived smaller and cold stars that emit light at longer optical waves, resulting in galaxies with red optical colours \citep{Kelson2001, Bell2004}. This is an indication that star formation activity shut downs on short timescales, a process also referred to as quenching. In Figure~\ref{fig:Colour_Bimodality}, we show the colour-magnitude distribution of local galaxies from the SDSS, in which the red (generally ETGs) and the blue (generally LTGs) sequences are identified \citep{Baldry2004}. Considering that the absolute magnitude is a measurement of the luminosity of a galaxy, which, in turn, can be related to luminous mass \citep{Kauffmann2003}, a bimodality in galaxy mass is also represented in this plot. Thus, ETGs tend to be more massive than LTGs. Finally, galaxies hosting AGNs have been shown to exhibit a bimodal distribution on the radio-optical plane, with radio-loud galaxies being about $10^3$ times brighter in radio than radio-quiet galaxies \citep{Kellermann1989, Xu1999, Sikora2007}. The radio-loudness bimodality is also linked to the galaxy morphological type, with radio-loud and radio-quiet objects tending to be red massive ETGs and blue LTGs, respectively\footnote{In addition, there is an important dependence on the BH accretion rate, e.g. ETGs hosting highly accretting BHs are usually radio-quiet.}.

The origin of these bimodalities is currently not fully understood as the assembly of the massive red early-type galaxies is still an open question. One possible explanation might lie in the different gas accretion modes for low-mass and high-mass dark matter haloes, with the former being dominated by cold filamentary accretion, whereas the latter by spherical hot accretion \citep{Silk1977, Rees1977, Keres2005, Dekel2006}. SMBH properties such as accretion rate and spin seem to have an important impact on the bimodal distribution of galaxy colour and radio-loudness as radio-loud galaxies often exhibit strong relativistic jets and AGN feedback-driven gas outflows \citep{Wilson1995, Hughes2003, Jester2005}. These processes regulate the availability of cold ISM gas for the star formation process and lead often to galaxy quenching and reddening.

%==================================================================================================
\section{Main sequence of star-forming galaxies}
\label{sec:Main_Sequence}
%==================================================================================================

The collapse of baryons into dark matter haloes and the subsequent radiative cooling and settling of the gas are followed by the formation of stars. If primordial gas is cooled down sufficiently, internal processes can lead to the formation of molecules, which eases a small scale fragmentation of the gas into clouds that can gravitationally collapse after loosing hydrostatic equilibrium and finally evolve into stars. The details of the collapse process strongly depend on the timescales over which the gas is able to cool down, which differ significantly between the first population of stars (so-called population III stars) and the more metal-rich population that are produced in later evolutionary stages (population I and II stars). This results in population III stars being more massive and less long-lived than present-day population I and II stars.

The overall star formation activity in the Universe evolves in time and becomes significantly high between $z\sim 1 - 4$, reaching its peak at about $z\sim 2$ (lookback time of about $10.5\, \mathrm{Gyr}$), as revealed by observations of the cosmic star formation rate density (cSFRD) \citep[see e.g.][]{Madau2014}. Furthermore, recent observational studies have shown that most of the actively star-forming galaxies follow a relation dubbed the main sequence, which links the assembled stellar content, i.e. the galaxy stellar mass $M_{\star}$, with the current ongoing star formation rate (SFR) \citep[e.g.][]{Noeske-2007, Daddi2007, Elbaz2007, Whitaker2012}. In Figure~\ref{fig:Main_Sequence}, we show the stellar mass and SFR of star-forming galaxies for different redshift ranges measured by \citet{Whitaker2012}. These observations evince the presence of the main sequence at different cosmic times. The normalisation of the relation, which is given by the specific star formation rate (sSFR) with $\mathrm{sSFR} = \frac{\mathrm{SFR}}{M_{\star}}$, is redshift-dependent due to the evolution of the cSFRD. Specifically, $\mathrm{sSFR}\propto (1+z)^3$, as shown by \citet{Lilly2013}. This indicates that, at a given stellar mass, SFRs at $z=0$ are lower by a factor of $\sim 10-20$ compared to those at $z=2$; in other words, present-day galaxies are less efficient in forming stars than galaxies at $z\sim2$. The relatively small scatter of the relation ($0.2 - 0.3\, \mathrm{dex}$) is interpreted as that star-forming galaxies evolve along the main sequence as they build up their stellar content, while keeping their sSFR at equilibrium. This implies the existence of different universal star formation regimes that operate at different cosmic times. Thus, galaxies at high redshifts grow their stellar content in a fast and continuous fashion. This is sustained by efficient feeding through cold accretion streams from the cosmic web and minor mergers. At low redshift, self-regulated feedback processes from stars and central black holes, inefficient cooling rates and gas exhaustion cause a drop in star formation efficiency, ultimately resulting in low SFRs \citep[for a thoughtful discussion of these processes, see][]{Schaye2010}.

%.........................................................................
%	Figure 3
%.........................................................................
%Main sequence of star-forming galaxies
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]
{./figures/2_properties_galaxies/Main_Sequence.pdf}
\caption{\small{Relation between stellar mass $M_{\star}$ and SFR of observed galaxies. Each panel shows galaxies measured within the NEWFIRM Medium-Band survey in different redshift ranges by \citet{Whitaker2012}. The symbols denote the running medians. In the bottom, right panel, vertical bars represent the scatter and dashed lines correspond to the best-fit power law to the medians. The main sequence of star-forming galaxies is ubiquitous, with only the normalisation varying in redshift. This figure is taken from \citet{Whitaker2012}.}}
\label{fig:Main_Sequence}
\end{figure}
%.........................................................................

In addition to the population of star-forming galaxies that are closely aligned on the main sequence, there exist other populations of galaxies that are outliers in the relation. For instance, star-bursting galaxies exhibit anomalously large SFRs that are $4-10$ times higher than main sequence counterparts. They are likely the result of transient strong interaction such as major mergers that drive gas inflows and compress the central gas, enhancing thus the star formation process \citep{Rodighiero2011}. On the other hand, the population of red sequence ETGs exhibits very low SFRs compared to main sequence galaxies as most of the star formation activity is quenched.

%==================================================================================================
\section{Stellar mass -- metallicity relation}
\label{sec:MZRelation}
%==================================================================================================

During the recombination era, when electrons become bound to nucleons, the baryonic content of the Universe is dominated by neutral hydrogen. Helium and small traces of lithium are also present as a result of primordial nucleosynthesis, which occurred about $10-20\, \mathrm{s}$ after the Big Bang. As discussed before, baryons collapse onto already-settled dark matter structures. Due to its collisional nature, the gas is heated up as the kinetic energy released during the infall is converted into thermal energy. Additionally, processes such as shock heating and photoionisation from local sources and from the ultraviolet background (UVB) can further heat up the gas. Nevertheless, in order to sustain the fragmentation and the small-scale gravitational collapse needed for forming stars, the gas has to be cooled down. 

For the composition of the primordial gas, the processes that drive the cooling are two-body radiative processes, e.g. collisional excitation, collisional ionisation and standard recombination of neutral hydrogen $\mathrm{H}^0$, neutral helium $\mathrm{He}^0$ and single ionised helium $\mathrm{He}^+$, dielectronic recombination of single ionised helium $\mathrm{He}^+$ and free-free emission (bremsstrah\-lung) \citep{Black1981, Cen1992}. The subsequent gravitational collapse of the gas leads to the formation of the first generation of stars in the Universe, i.e. the population III stars. These massive stars have short lifetimes as they burn their hydrogen supply through thermonuclear fusion processes very quickly, resulting in a rapid evolution and a chemical enrichment of the stellar interior. They end up as core-collapse supernovae, releasing thus a significant amount of metal-enriched gas\footnote{In astronomy, elements other than $\mathrm{H}$ and $\mathrm{He}$ are often referred to as ``metals''.} to the surroundings.

As the chemical composition of the ISM evolves, new cooling channels become available, namely the metal-line emissions \citep{Wiersma2009}. This allows for the gas to undergo even further fragmentation, thereby preventing the formation of more population III stars, but triggering instead the formation of less massive, more long-lived, metal-rich populations I and II starts. During the evolution and final stages of these late stellar populations, the ISM is chemically enriched with heavier elements, including trans-iron elements that are only produced by supernovae.

Thus, it is theoretically predicted that the assembly of the stellar content of galaxies is accompanied by a chemical evolution of the ISM. Observations of oxygen abundances of H~II (ionised atomic hydrogen) regions in local galaxies have revealed a relation between gas-phase metallicity and luminosity \citep{Skillman1989, Zaritsky1994}. This has been recently confirmed to be a manifestation of a more fundamental stellar mass -- metallicity relation \citep[MZR;][]{Lequeux1979, Tremonti-2004}. In Figure~\ref{fig:MZ_Relation}, we show the local MZR for star-forming galaxies of the SDSS at $z\sim 0.1$ compiled by \citet{Tremonti-2004}. The small scatter of the residual distribution ($0.1\, \mathrm{dex}$) indicates that star-forming galaxies evolve along the MZR, thereby supporting the theoretical predictions. At stellar masses above $10^{10.5}\, \mathrm{M}_{\odot}$, the evolution of the gas metallicity can be treated as a ``closed box model'', in which gas inflows and outflows can be neglected. Therefore, the metallicity is only controlled by gas recycling and star formation. Considering that at this range of stellar mass, AGN quenching processes start to become relevant, the relation tends to flatten. In the stellar mass range $10^{8.5}-10^{10.5}\, \mathrm{M}_{\odot}$, the relation is relatively steep due to presence of strong winds driven by supernovae and AGB stars. The winds are loaded with metals, reducing thus the gas metallicity when they leave the galaxy. This scenario can be thought as a ``leaky box model''.

%.........................................................................
%	Figure 4
%.........................................................................
%Mass metallicity relation
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]
{./figures/2_properties_galaxies/MZ_Relation.png}
\caption{\small{Relation between stellar mass and gas-phase oxygen abundance of star-forming galaxies in the SDSS. Diamonds correspond to medians in different mass bins. Solid lines represent the $68\%$ and $95\%$ percentiles. The red line is a polynomial fit to the data. This figure is taken from \citet{Tremonti-2004}.}}
\label{fig:MZ_Relation}
\end{figure}
%.........................................................................

In addition to the the main dependence of the gas-phase metallicity on stellar mass, a secondary dependence on the SFR has been found in SDSS galaxies \citep[see e.g.][]{Ellison-2008, Mannucci-2010}. This is encoded in the so-called fundamental metallicity relation (FMR), which shows that, at a fixed stellar mass, galaxies with higher SFRs tend to have lower metallicities. This behaviour is likely an effect of the complex interplay between accretion of pristine gas and stellar chemical enrichment. Although steadily evolving, relatively isolated star-forming galaxies are well aligned on the FMR, galaxies undergoing major and intermediate mergers have been shown to be a prominent outlier population in the relation, displaying lower-than-expected metallicities \citep{Mannucci-2010, Gronnow-2015}. In part II of this thesis, we investigate the properties of the outlier population of interacting galaxies in both simulations and observations.
